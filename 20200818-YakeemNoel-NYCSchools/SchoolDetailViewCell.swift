//
//  SchoolDetailViewCell.swift
//  20200818-YakeemNoel-NYCSchools
//
//  Created by Yakeem Noel on 8/19/20.
//  Copyright © 2020 Yakeem Noel. All rights reserved.
//

import UIKit

class SchoolDetailViewCell: UITableViewCell {
    enum DetailItem: String, CaseIterable {
        case header = ""
        case overviewParagraph = "Overview"
        case SATscores = "SAT Scores"
        case location = "Location"
        case phoneNumber = "Phone Number"
        case schoolEmail = "Email"
        case website = "Website"
    }

    class var identifier: String {
        return String(describing: self)
    }

    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.gray.withAlphaComponent(0.65)
        contentView.addSubview(view)
        return view
    }()

    private lazy var mainLabel: UILabel = {
        let label = SchoolPreviewTableViewCell.rawLabel
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        label.textColor = .darkText
        contentView.addSubview(label)
        return label
    }()


    private lazy var subLabel: UILabel = {
        let label = SchoolPreviewTableViewCell.rawLabel
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textColor = .darkGray
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.adjustsFontSizeToFitWidth = false
        containerView.addSubview(label)
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupCell() {
        backgroundColor = .clear
        containerView.backgroundColor = .clear
        selectionStyle = .none
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.layer.cornerRadius = 8.0
    }

    private func setupConstraints() {
        containerView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(CGFloat.standardPadding)
            make.trailing.equalToSuperview().offset(CGFloat.standardPadding.inverse)
            make.bottom.equalToSuperview().offset(CGFloat.standardPadding.inverse)
        }

        mainLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(CGFloat.standardPadding)
            make.leading.equalToSuperview().offset(CGFloat.standardPadding)
            make.trailing.equalToSuperview().offset(CGFloat.standardPadding.inverse)
            make.bottom.equalTo(containerView.snp.top).offset(CGFloat.standardPadding.inverse / 2)
            make.height.equalTo(mainLabel.font.lineHeight)
        }

        subLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(CGFloat.standardPadding)
            make.leading.equalToSuperview().offset(CGFloat.standardPadding)
            make.trailing.equalToSuperview().offset(CGFloat.standardPadding.inverse)
            make.bottom.equalToSuperview().offset(CGFloat.standardPadding.inverse)
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }

    func configure(with data: NYCSchoolModel, detailItem: DetailItem, scores: NYCSChoolsSATScores? = nil) {
        mainLabel.text = detailItem.rawValue
        switch detailItem {
        case .overviewParagraph:
            subLabel.text = data.overviewParagraph
        case .SATscores:
            guard
                let testTakers = scores?.numberOfTestTakers,
                let mathScore = scores?.mathScores,
                let writingScore = scores?.writingScores,
                let readingScore = scores?.readingScores
            else { return }
            let satScores = """
            Number of SAT test takers: \(testTakers)
            Math Avarage Score: \(mathScore)
            Writing Avarage Score: \(writingScore)
            Reading Avarage Score: \(readingScore)
            """
            subLabel.text = satScores
        case .location:
            subLabel.text = data.location
        case .phoneNumber:
            subLabel.text = data.phoneNumber
        case .schoolEmail:
            subLabel.text = data.schoolEmail
        case .website:
            subLabel.text = data.website
        case .header:
            subLabel.text = nil
        }
    }
}
