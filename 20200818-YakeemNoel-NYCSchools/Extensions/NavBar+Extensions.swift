//
//  NavBar+Extensions.swift
//  20200818-YakeemNoel-NYCSchools
//
//  Created by Yakeem Noel on 8/18/20.
//  Copyright © 2020 Yakeem Noel. All rights reserved.
//

import UIKit

extension UINavigationBar {

    func navStyle() {
        prefersLargeTitles = true
        tintColor = UIColor.lightGray
        barTintColor = UIColor.black
        largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
    }
}
