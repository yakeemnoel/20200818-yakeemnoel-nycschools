//
//  CGFloat+Extensions.swift
//  20200818-YakeemNoel-NYCSchools
//
//  Created by Yakeem Noel on 8/18/20.
//  Copyright © 2020 Yakeem Noel. All rights reserved.
//

import UIKit

extension CGFloat {
    static let imageViewSize = CGFloat(42.0)
    static let standardPadding = CGFloat(16.0)
    static let largePadding = CGFloat(32.0)
    static let mtaLineSpacing: CGFloat = 4.0

    var inverse: CGFloat {
        return self * -1
    }
}
