//
//  MTALine.swift
//  20200818-YakeemNoel-NYCSchools
//
//  Created by Yakeem Noel on 8/18/20.
//  Copyright © 2020 Yakeem Noel. All rights reserved.
//

import UIKit
import SnapKit

class MTALineView: UIView {
    enum Line: String, Decodable {
        case a, c, e, b, d, f, m, g, j, z, l, n, q, r, s, one, two, three, four, five, six, seven

        init?(with rawLine: String) {
            var line = rawLine.lowercased()
            switch rawLine {
            case "1":
                line = "one"
            case "2":
                line = "two"
            case "3":
                line = "three"
            case "4":
                line = "four"
            case "5":
                line = "five"
            case "6":
                line = "six"
            case "7":
                line = "seven"
            default:
                break
            }
            self.init(rawValue: line)
        }

        var name: String {
            switch self {
            case .a, .c, .e, .b, .d, .f, .m, .g, .j, .z, .l, .n, .q, .r, .s:
                return rawValue.uppercased()
            case .one:
                return "1"
            case .two:
                return "2"
            case .three:
                return "3"
            case .four:
                return "4"
            case .five:
                return "5"
            case .six:
                return "6"
            case .seven:
                return "7"
            }
        }

        var color: UIColor {
            switch self {
            case .a, .c, .e:
                return UIColor(hexString: "0039A6")
            case .b, .d, .f, .m:
                return UIColor(hexString: "FF6319")
            case .g:
                return UIColor(hexString: "6CBE45")
            case .j, .z:
                return UIColor(hexString: "996633")
            case .l:
                return UIColor(hexString: "A7A9AC")
            case .n, .q, .r:
                return UIColor(hexString: "FCCC0A")
            case .s:
                return UIColor(hexString: "808183")
            case .one, .two, .three:
                return UIColor(hexString: "EE353E")
            case .four, .five, .six:
                return UIColor(hexString: "00933C")
            case .seven:
                return UIColor(hexString: "B933AD")
            }
        }
    }

    private let lineLabel: UILabel = {
        let v = UILabel()
        v.textColor = .white
        v.clipsToBounds = true
        v.lineBreakMode = .byClipping
        v.textAlignment = .center
        v.translatesAutoresizingMaskIntoConstraints = false
        v.font = UIFont.systemFont(ofSize: 12.0, weight: .medium)
        return v
    }()

    let line: Line
    init(with line: Line) {
        self.line = line
        super.init(frame: .zero)
        setupView()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
    }

    private func setupView() {
        clipsToBounds = true
        backgroundColor = line.color
        lineLabel.text = line.name
        addSubview(lineLabel)
    }

    private func setupConstraints() {
        lineLabel.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
    }

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: 16.0, height: 16.0)
    }
}
