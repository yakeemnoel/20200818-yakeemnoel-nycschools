//
//  SchoolPreviewTableViewCell.swift
//  20200818-YakeemNoel-NYCSchools
//
//  Created by Yakeem Noel on 8/18/20.
//  Copyright © 2020 Yakeem Noel. All rights reserved.
//

import UIKit
import SnapKit

class SchoolPreviewTableViewCell: UITableViewCell {

    class var identifier: String {
        return String(describing: self)
    }

    class var rawLabel: UILabel {
        let label = UILabel()
        label.textColor = .darkText
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.adjustsFontForContentSizeCategory = true
        return label
    }

    var trainLines: [MTALineView.Line] = []

    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.65)
        view.clipsToBounds = true
        contentView.addSubview(view)
        return view
    }()

    private lazy var mtaLineView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = .mtaLineSpacing
        stackView.distribution = .fillProportionally
        stackView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(stackView)
        return stackView
    }()

    private lazy var schoolNameLabel: UILabel = {
        let label = SchoolPreviewTableViewCell.rawLabel
        label.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        label.textColor = .black
        label.numberOfLines = 3
        label.lineBreakMode = .byWordWrapping
        containerView.addSubview(label)
        return label
    }()

    private lazy var schoolNeighborhoodLabel: UILabel = {
        let label = SchoolPreviewTableViewCell.rawLabel
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textColor = UIColor.black.withAlphaComponent(0.85)
        containerView.addSubview(label)
        return label
    }()

    private lazy var schoolGradesLabel: UILabel = {
        let label = SchoolPreviewTableViewCell.rawLabel
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textColor = UIColor.black.withAlphaComponent(0.50)
        containerView.addSubview(label)
        return label
    }()

    private lazy var totalStudentsLabel: UILabel = {
        let label = SchoolPreviewTableViewCell.rawLabel
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textColor = UIColor.black.withAlphaComponent(0.50)
        containerView.addSubview(label)
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupCell() {
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        selectionStyle = .none
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.layer.cornerRadius = 8.0
    }

    private func setupConstraints() {
        containerView.snp.remakeConstraints { (make) in
            make.top.equalToSuperview().offset(CGFloat.standardPadding)
            make.leading.equalToSuperview().offset(CGFloat.standardPadding)
            make.trailing.equalToSuperview().offset(CGFloat.standardPadding.inverse)
            make.bottom.equalToSuperview().offset(CGFloat.standardPadding.inverse)
        }

        schoolNameLabel.snp.remakeConstraints { (make) in
            make.top.equalTo(containerView.snp.top).offset(CGFloat.standardPadding)
            make.leading.equalTo(containerView.snp.leading).offset(CGFloat.standardPadding)
            make.trailing.equalTo(containerView.snp.trailing).inset(CGFloat.standardPadding)
        }

        schoolNeighborhoodLabel.snp.remakeConstraints { (make) in
            make.top.equalTo(schoolNameLabel.snp.bottom)
            make.leading.equalTo(schoolNameLabel.snp.leading)
            make.trailing.equalTo(schoolNameLabel.snp.trailing)
        }

        mtaLineView.snp.remakeConstraints { (make) in
            make.top.equalTo(schoolNeighborhoodLabel.snp.bottom)
            make.leading.equalTo(schoolNeighborhoodLabel.snp.leading)
            make.height.greaterThanOrEqualTo(24.0)
            make.width.equalTo((24 * max(1, trainLines.count)) + (Int(CGFloat.mtaLineSpacing) * max(1, trainLines.count)))
        }

        schoolGradesLabel.snp.remakeConstraints { (make) in
            make.top.equalTo(mtaLineView.snp.bottom)
            make.leading.equalTo(schoolNeighborhoodLabel.snp.leading)
            make.trailing.equalTo(totalStudentsLabel.snp.leading).offset(-CGFloat.standardPadding)
            make.bottom.equalToSuperview().offset(-CGFloat.standardPadding)
        }

        totalStudentsLabel.snp.remakeConstraints { (make) in
            make.top.equalTo(schoolGradesLabel.snp.top)
            make.trailing.equalTo(schoolNameLabel.snp.trailing).priority(.low)
            make.bottom.equalTo(schoolGradesLabel.snp.bottom)
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        mtaLineView.arrangedSubviews.forEach { (v) in
            v.isHidden = true
            mtaLineView.removeArrangedSubview(v)
        }
        schoolNameLabel.snp.removeConstraints()
        schoolNeighborhoodLabel.snp.removeConstraints()
        schoolGradesLabel.snp.removeConstraints()
        totalStudentsLabel.snp.removeConstraints()
        mtaLineView.snp.removeConstraints()
    }

    func configure(with data: NYCSchoolModel) {
        schoolNameLabel.text = data.schoolName
        schoolNeighborhoodLabel.text = data.neighborhood
        var gradeLevels = "N/A"
        if let _gradeLevels = data.gradeLevels, !_gradeLevels.contains("School") {
            gradeLevels = _gradeLevels
        }
        let schoolGradesMainAttributedString = NSMutableAttributedString(string: "Grades: ")
        let schoolGradesAttributedString = NSAttributedString(string: gradeLevels, attributes: [
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0, weight: .medium)
        ])
        schoolGradesMainAttributedString.append(schoolGradesAttributedString)
        schoolGradesLabel.attributedText = schoolGradesMainAttributedString

        if let totalStudents = data.studentSum {
            let totalStudentsMainAttributedString = NSMutableAttributedString(string: "Total Students: ")
            let totalStudentsAttributedString = NSAttributedString(string: "\(totalStudents)", attributes: [
                NSAttributedString.Key.foregroundColor: UIColor.black,
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0, weight: .medium)
            ])
            totalStudentsMainAttributedString.append(totalStudentsAttributedString)
            totalStudentsLabel.attributedText = totalStudentsMainAttributedString
        }

        trainLines = data.trainLines
        trainLines.forEach { (line) in
            let mtaLine = MTALineView(with: line)
            mtaLine.translatesAutoresizingMaskIntoConstraints = false
            mtaLine.sizeToFit()
            mtaLineView.addArrangedSubview(mtaLine)
        }
        mtaLineView.sizeToFit()
        setupConstraints()
    }
}
