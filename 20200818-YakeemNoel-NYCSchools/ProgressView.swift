//
//  ProgressView.swift
//  20200818-YakeemNoel-NYCSchools
//
//  Created by Yakeem Noel on 8/19/20.
//  Copyright © 2020 Yakeem Noel. All rights reserved.
//

import SnapKit

final class ProgressView: UIView {

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.font = UIFont.systemFont(ofSize: 18.0, weight: .medium)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.adjustsFontForContentSizeCategory = true
        label.text = "Fetching Schools..."
        return label
    }()

    private let progressView: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(style: .large)
        view.color = .white
        return view
    }()

    convenience init() {
        self.init(frame: .zero)
        setupView()
        setupConstraints()
    }

    private func setupView() {
        backgroundColor = UIColor(hexString: "2d3436")
        isUserInteractionEnabled = false
        layer.cornerRadius = 8.0
        addSubview(titleLabel)
        addSubview(progressView)
    }

    private func setupConstraints() {
        progressView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }

        titleLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(progressView.snp.top).offset((CGFloat.standardPadding / 3.0).inverse)
            make.centerX.equalTo(progressView.snp.centerX)
            make.height.equalTo(titleLabel.font.lineHeight + 4.0) // 4.0 points of padding
        }
    }

    func start() {
        if !progressView.isAnimating {
            progressView.startAnimating()
        }
    }

    func stop() {
        if progressView.isAnimating {
            progressView.stopAnimating()
        }
    }
}

