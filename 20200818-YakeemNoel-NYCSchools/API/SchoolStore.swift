//
//  SchoolStore.swift
//  20200818-YakeemNoel-NYCSchools
//
//  Created by Yakeem Noel on 8/19/20.
//  Copyright © 2020 Yakeem Noel. All rights reserved.
//

import Moya

extension Notification.Name {
    static let progressUpdate = Notification.Name("SchoolStoreProgressUpdateNotification")
    static let schoolStoreDidFetchSchools = Notification.Name("SchoolStoreDidFetchSchoolsNotification")
    static let schoolStoreDidFailToLoadSchools = Notification.Name("SchoolStoreDidFailToLoadSchoolsNotification")
}

enum SchoolStoreError: Error {
    case objectDecodingFailed(URL?)
    case requestError(Error)

    var localizedDescription: String {
        switch self {
        case .objectDecodingFailed(let url):
            guard let url = url else { return "No reason found." }
            return "Failed to decode response payload. @: \(url.absoluteString))"
        case .requestError(let error):
            return error.localizedDescription
        }
    }
}

final class SchoolStore {
    static let shared = SchoolStore()

    fileprivate(set) var schools = [NYCSchoolModel]()
    fileprivate(set) var satScores = [NYCSChoolsSATScores]()
    private let requestQueue = DispatchQueue(label: "com.schoolStore.request.queue", qos: .userInitiated)

    private init() {
        requestQueue.async { [weak self] in
            guard let self = self else { return }
            let provider = MoyaProvider<NYCSchoolsAPI>()
            provider.request(.testScores) { (results) in
                switch results {
                case .success(let response):
                    guard let testScores = response.decodedObject(type: [NYCSChoolsSATScores].self) else { return }
                    self.satScores = testScores
                case .failure(let error):
                    print("Error loading scores: \(error.localizedDescription)")
                }
            }
            provider.request(.schools, callbackQueue: DispatchQueue.main, progress: { (progress) in
                NotificationCenter.default.post(name: .progressUpdate, object: progress)
            }) { (result) in
                switch result {
                case .success(let response):
                    guard let schools = response.decodedObject(type: [NYCSchoolModel].self) else {
                        NotificationCenter.default.post(name: .schoolStoreDidFailToLoadSchools, object: SchoolStoreError.objectDecodingFailed(response.request?.url))
                        return
                    }
                    self.schools = schools
                    NotificationCenter.default.post(name: .schoolStoreDidFetchSchools, object: nil)
                case .failure(let error):
                    NotificationCenter.default.post(name: .schoolStoreDidFailToLoadSchools, object: SchoolStoreError.requestError(error))
                }
            }
        }
    }

    func scoreFor(dbn: String) -> NYCSChoolsSATScores? {
        return satScores.filter {  $0.dbn == dbn }.first
    }
}
