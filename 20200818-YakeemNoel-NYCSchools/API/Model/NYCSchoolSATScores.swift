//
//  NYCSchoolSATScores.swift
//  20200819-YakeemNoel-NYCSchools
//
//  Created by Yakeem Noel on 8/19/20.
//  Copyright © 2020 Yakeem Noel. All rights reserved.
//

import Foundation

struct NYCSChoolsSATScores: Decodable {
    let dbn: String?
    let mathScores: String?
    let readingScores: String?
    let writingScores: String?
    let numberOfTestTakers: String?

    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case mathScores = "sat_math_avg_score"
        case readingScores = "sat_critical_reading_avg_score"
        case writingScores = "sat_writing_avg_score"
        case numberOfTestTakers = "num_of_sat_test_takers"
    }
}



