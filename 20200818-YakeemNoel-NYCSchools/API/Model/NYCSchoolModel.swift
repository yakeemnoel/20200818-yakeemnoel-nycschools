//
//  NYCSchoolModel.swift
//  20200818-YakeemNoel-NYCSchools
//
//  Created by Yakeem Noel on 8/18/20.
//  Copyright © 2020 Yakeem Noel. All rights reserved.
//

import Foundation

struct NYCSchoolModel: Decodable {
    let dbn: String?
    let schoolName: String?
    let overviewParagraph: String?
    let neighborhood: String?
    let buildingCode: String?
    let location: String?
    let phoneNumber: String?
    let schoolEmail: String?
    let website: String?
    let subway: String?
    let studentSum: String?
    let gradeLevels: String?
    let latitude: String?
    let longitude: String?

    var coordinates: String {
        guard let lat = latitude, let longitude = longitude else { return "" }
        return "\(lat),\(longitude)"
    }

    var trainLines: [MTALineView.Line] {
        guard let subways = subway, subways != "N/A" else { return [] }
        let firstSetOfLines: [MTALineView.Line] = subways.components(separatedBy: ", ").compactMap { (rawLine) in
            guard let firstChar = rawLine.first else {
                return nil
            }
            return MTALineView.Line(with: String(firstChar))
        }
        let secondSetOfLines: [MTALineView.Line] = subways.components(separatedBy: ", ").compactMap { (rawLine) in
            guard rawLine.contains(";"), let lastChar = rawLine.last else {
                return nil
            }
            return MTALineView.Line(with: String(lastChar))
        }
        return firstSetOfLines + secondSetOfLines
    }

    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case neighborhood = "neighborhood"
        case buildingCode = "building_code"
        case location = "location"
        case phoneNumber = "phone_number"
        case schoolEmail = "school_email"
        case website = "website"
        case subway = "subway"
        case studentSum = "total_students"
        case gradeLevels = "finalgrades"
        case latitude = "latitude"
        case longitude = "longitude"
    }
}
