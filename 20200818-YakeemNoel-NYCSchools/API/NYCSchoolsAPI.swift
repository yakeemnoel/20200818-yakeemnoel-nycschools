//
//  NYCSchoolsAPI.swift
//  20200818-YakeemNoel-NYCSchools
//
//  Created by Yakeem Noel on 8/18/20.
//  Copyright © 2020 Yakeem Noel. All rights reserved.
//

import Foundation
import Moya

enum NYCSchoolsAPI {
    case schools
    case testScores
}


extension NYCSchoolsAPI: TargetType {
    var path: String {
        ""
    }

    var method: Moya.Method {
        .get
    }

    var sampleData: Data {
        Data()
    }

    var task: Task {
        .requestPlain
    }

    var headers: [String : String]? {
        [:]
    }

    var baseURL: URL {
        var urlString = ""
        switch self {
        case .schools:
            urlString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        case .testScores:
            urlString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
        }
        guard let url = URL(string: urlString) else {
            fatalError("Invalid baseUrl for NYC Schools or NYC SAT scores.")
        }
        return url
    }
}

extension Response {
    func decodedObject<T>(type: T.Type) -> T? where T: Decodable {
        let jsonDecoder = JSONDecoder()
        do {
            return try jsonDecoder.decode(T.self, from: data)
        } catch {
            print("decodedObject error: \(error.localizedDescription)")
            return nil
        }
    }
}
