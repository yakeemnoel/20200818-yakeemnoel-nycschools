//
//  DetailViewController.swift
//  20200818-YakeemNoel-NYCSchools
//
//  Created by Yakeem Noel on 8/19/20.
//  Copyright © 2020 Yakeem Noel. All rights reserved.
//

import UIKit
import SafariServices

class DetailViewController: UIViewController {

    private let schoolDetail: NYCSchoolModel
    private let detailItems = SchoolDetailViewCell.DetailItem.allCases

    private let tableView = UITableView()

    init(with data: NYCSchoolModel) {
        schoolDetail = data
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

// MARK: - View Controller Life Cycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
        setupConstraints()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }

// MARK:- Required setup Functions
    private func setupController() {
        view.backgroundColor = .white
        setupNavationBar()

        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(SchoolPreviewTableViewCell.self, forCellReuseIdentifier: SchoolPreviewTableViewCell.identifier)
        tableView.register(SchoolDetailViewCell.self, forCellReuseIdentifier: SchoolDetailViewCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        view.addSubview(tableView)
    }

    private func setupConstraints() {
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    private func setupNavationBar() {
        title = "School Details"
        navigationController?.navigationBar.navStyle()
    }
}

extension DetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400.0
    }
}

extension DetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detailItems.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let detailItem = detailItems[indexPath.row]
        if detailItem == .header {
            let cell = tableView.dequeueReusableCell(withIdentifier: SchoolPreviewTableViewCell.identifier, for: indexPath) as! SchoolPreviewTableViewCell
            cell.configure(with: schoolDetail)
            return cell
        } else if detailItem == .SATscores {
            let cell = tableView.dequeueReusableCell(withIdentifier: SchoolDetailViewCell.identifier, for: indexPath) as! SchoolDetailViewCell
            cell.configure(with: schoolDetail, detailItem: detailItem, scores: SchoolStore.shared.scoreFor(dbn: schoolDetail.dbn ?? ""))
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: SchoolDetailViewCell.identifier, for: indexPath) as! SchoolDetailViewCell
            cell.configure(with: schoolDetail, detailItem: detailItem)
            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailItem = detailItems[indexPath.row]
        switch detailItem {
        case .location:
            guard let url = URL(string: "http://maps.apple.com/?q=\(schoolDetail.coordinates)") else { return }
            UIApplication.shared.open(url)
            break
        case .phoneNumber:
            guard let phoneNumber = schoolDetail.phoneNumber, let url = URL(string: "tel:\(phoneNumber)") else { return }
            UIApplication.shared.open(url)
        case .schoolEmail:
            guard let email = schoolDetail.schoolEmail, let url = URL(string: "mailto:\(email)") else { return }
            UIApplication.shared.open(url)
        case .website:
            guard let website = schoolDetail.website, let url = URL(string: "http://\(website)") else { return }
            let safariController = SFSafariViewController(url: url)
            safariController.modalPresentationStyle = .overCurrentContext
            present(safariController, animated: true, completion: nil)
        default:
            return
        }
    }
}
