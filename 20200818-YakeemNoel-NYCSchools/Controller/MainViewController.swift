//
//  MainViewController.swift
//  20200818-YakeemNoel-NYCSchools
//
//  Created by Yakeem Noel on 8/18/20.
//  Copyright © 2020 Yakeem Noel. All rights reserved.
//

import UIKit
import SnapKit
import Moya

class MainViewController: UIViewController {

// MARK: - UIView Properties
    private let tableView = UITableView()
    private var fetchProgressView: ProgressView? = ProgressView()

    var schools: [NYCSchoolModel] {
        return schoolStore?.schools ?? []
    }

    var scores: [NYCSChoolsSATScores] {
        return schoolStore?.satScores ?? []
    }

    private var schoolStore: SchoolStore?
    var selectedCell: SchoolPreviewTableViewCell?

// MARK: - View Controller Life Cycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupController()
        setupConstraints()
    }

// MARK:- Required setup Functions
    private func setupController() {
        view.backgroundColor = .white
        setupNavigationBar()

        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(SchoolPreviewTableViewCell.self, forCellReuseIdentifier: SchoolPreviewTableViewCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)

        let progressView = ProgressView()
        progressView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(progressView)
        view.bringSubviewToFront(progressView)
        self.fetchProgressView = progressView
        progressView.start()

        NotificationCenter.default.addObserver(self, selector: #selector(onDidFetchSchools(notification:)), name: .schoolStoreDidFetchSchools, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onFetchSchoolsProgressUpdate(notification:)), name: .progressUpdate, object: nil)

        schoolStore = SchoolStore.shared
    }

    private func setupConstraints() {
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        fetchProgressView?.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.height.equalTo(UIScreen.main.bounds.height / 3)
            make.leading.equalToSuperview().offset(CGFloat.standardPadding)
            make.trailing.equalToSuperview().offset(CGFloat.standardPadding.inverse)
        }
    }

    private func setupNavigationBar() {
        title = "NYC Schools"
        navigationController?.navigationBar.navStyle()
    }

    @objc private func onDidFetchSchools(notification: Notification) {
        tableView.reloadData()
    }

    @objc private func onFetchSchoolsProgressUpdate(notification: Notification) {
        guard let progress = notification.object as? ProgressResponse else { return }
        if progress.completed {
            fetchProgressView?.stop()
            fetchProgressView?.isHidden = true
            fetchProgressView?.removeFromSuperview()
            view.bringSubviewToFront(tableView)
            fetchProgressView = nil
        }
    }
}

// MARK: - UITableViewDelegate
extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 700.0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedSchool = schools[indexPath.row]
        let detailVC = DetailViewController(with: selectedSchool)
        detailVC.modalPresentationStyle = .overCurrentContext
        navigationController?.pushViewController(detailVC, animated: true)
    }
}

// MARK: - UITableViewDataSource
extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SchoolPreviewTableViewCell.identifier, for: indexPath) as! SchoolPreviewTableViewCell
        guard schools.indices.contains(indexPath.row) else { return UITableViewCell() }
        let school = schools[indexPath.row]
        cell.configure(with: school)
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools.count
    }
}
