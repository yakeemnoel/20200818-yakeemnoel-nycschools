# NYC Schools
> Yakeem Noel

## Libraries

- Moya *( For simplified networking request )*
- SnapKit *( DSL for auto-layout )*


## Overview

The goal of this project is to create a simple app that displays a list of NYC High Schools and a more detailed view of a selected School. The details screen should also display if available SAT score stats for the selected school.

Selecting a school will show additional information about the school.

##### This iOS application displays data from the following API's:
~~~
- https://data.cityofnewyork.us/resource/f9bf-2cp4.json
- https://data.cityofnewyork.us/resource/s3k6-pzi2.json
~~~
## Implementation Preview

**Main Screen** | **Detail View**
-|-
![alt text](Assets/IMG_0924.PNG "Main screen") | ![alt text](Assets/IMG_0925.PNG "Detail screen")
